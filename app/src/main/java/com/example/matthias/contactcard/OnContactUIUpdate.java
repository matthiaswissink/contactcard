package com.example.matthias.contactcard;

/**
 * Created by Matthias on 04-Oct-17.
 */

public interface OnContactUIUpdate {

    void onContactUIUpdate();

}
