package com.example.matthias.contactcard;

/**
 * Created by Matthias on 04-Oct-17.
 */

public interface OnContactItemReady {

    void onContactReady(Contact contact);

}
