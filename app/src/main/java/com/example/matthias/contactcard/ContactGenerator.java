package com.example.matthias.contactcard;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import java.util.ArrayList;

//import static android.content.Context.MODE_PRIVATE;
//import static android.database.sqlite.SQLiteDatabase.openOrCreateDatabase;

/**
 * Created by Matthias on 04-Oct-17.
 */

public class ContactGenerator implements OnContactItemReady {

    private OnContactUIUpdate onContactUIUpdate;
    private ArrayList<Contact> contacts = new ArrayList<>();
    private Context context;

    public ContactGenerator(OnContactUIUpdate onContactUIUpdate, Context context) {
        this.onContactUIUpdate = onContactUIUpdate;
        this.context = context;
    }

    @Override
    public void onContactReady(Contact contact) {
        new ContactDB().execute(contact);
        this.contacts.add(contact);
        onContactUIUpdate.onContactUIUpdate();
    }

    public void getNewContact() {
        String URL = "https://randomuser.me/api/";
        String[] urls = new String[]{URL};
        ContactTask contactTask = new ContactTask(this);
        contactTask.execute(urls);
    }

    public void addContactToDb(Contact contact) {


    }

    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    public ArrayList<Contact> getContactsFromDB() {

        ArrayList<Contact> contacts = new ArrayList<>();

        try {
            SQLiteDatabase myDatabase = context.openOrCreateDatabase("Contacts", 0, null);
            Cursor resultSet = myDatabase.rawQuery("Select * from Contact", null);
            resultSet.moveToLast();
            int count = resultSet.getPosition();
            resultSet.moveToFirst();
            for (int i = 1; i <= count; i++) {

                Contact contact = new Contact(
                        resultSet.getString(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getString(5),
                        resultSet.getString(6),
                        resultSet.getString(7),
                        resultSet.getString(8),
                        resultSet.getString(9)
                );

                contacts.add(contact);

                if (i == count) {
                    resultSet.close();
                } else {
                    resultSet.moveToNext();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return contacts;
    }

    private class ContactDB extends AsyncTask<Contact, Void, Void> {

        @Override
        protected Void doInBackground(Contact... contacts) {
            Contact contact = contacts[0];

            SQLiteDatabase myDatabase = context.openOrCreateDatabase("Contacts", 0, null);

            if (myDatabase.isOpen()) {
                myDatabase.execSQL("CREATE TABLE IF NOT EXISTS Contact(firstName VARCHAR, lastName VARCHAR, street VARCHAR, city VARCHAR, email VARCHAR, phone VARCHAR, imageThumb VARCHAR, imageMed VARCHAR, image VARCHAR);");
                myDatabase.execSQL("INSERT INTO Contact VALUES('"
                        + contact.getFirstName() + "', '"
                        + contact.getLastName() + "', '"
                        + contact.getStreet() + "', '"
                        + contact.getCity() + "', '"
                        + contact.getEmail() + "', '"
                        + contact.getPhone() + "', '"
                        + contact.getImageThumb() + "', '"
                        + contact.getImageMed() + "', '"
                        + contact.getImage() + "');");

                Log.i("DATABASE", "Contact added to database");

                myDatabase.close();
            }
            return null;
        }
    }

}
