package com.example.matthias.contactcard;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Objects;

import static android.content.ContentValues.TAG;

/**
 * Created by Matthias on 04-Oct-17.
 */

public class ContactTask extends AsyncTask<String, Void, String>{

    private OnContactItemReady onContactItemReady;

    public ContactTask(ContactGenerator onContactItemReady) {
        this.onContactItemReady = onContactItemReady;
    }

    @Override
    protected String doInBackground(String... strings) {
        InputStream inputStream = null;
        BufferedReader reader = null;
        String urlString = "";
        String response = "";

        try {
            URL url = new URL(strings[0]);
            URLConnection connection = url.openConnection();

            reader = new BufferedReader(
                    new InputStreamReader(
                            connection.getInputStream()));
            response = reader.readLine();
            String line;
            while ((line = reader.readLine()) != null) {
                response += line;
            }
        } catch (MalformedURLException e) {
            Log.e("TAG", e.getLocalizedMessage());
            return null;
        } catch (IOException e) {
            Log.e("TAG", e.getLocalizedMessage());
            return null;
        } catch (Exception e) {
            Log.e("TAG", e.getLocalizedMessage());
            return null;
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    Log.e("TAG", e.getLocalizedMessage());
                    return null;
                }
            }
        }

        return response;
    }


    protected void onPostExecute(String response) {

        if(response == null || Objects.equals(response, "")) {
            return;
        }

        JSONObject jsonObject;

        try {
            jsonObject = new JSONObject(response);

            JSONArray users = jsonObject.getJSONArray("results");
            for(int i = 0; i < users.length(); i++) {

                JSONObject user = users.getJSONObject(i);

                JSONObject name = user.getJSONObject("name");
                String firstName = name.getString("first");
                String lastName = name.getString("last");

                JSONObject location = user.getJSONObject("location");
                String street = location.getString("street");
                String city = location.getString("city");

                String email = user.getString("email");

                String phone = user.getString("phone");

                JSONObject picture = user.getJSONObject("picture");
                String image = picture.getString("large");
                String imageMed = picture.getString("medium");
                String imageThumb = picture.getString("thumbnail");

                Contact contact = new Contact(firstName, lastName, street, city, email, phone,imageThumb, imageMed, image);

                onContactItemReady.onContactReady(contact);

            }
        } catch( JSONException ex) {
            Log.e(TAG, "onPostExecute JSONException " + ex.getLocalizedMessage());
        }
    }
}
