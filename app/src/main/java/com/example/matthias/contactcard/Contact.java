package com.example.matthias.contactcard;

import java.io.Serializable;

/**
 * Created by Matthias on 04-Oct-17.
 */

public class Contact implements Serializable{

    private String firstName;
    private String lastName;
    private String street;
    private String city;
    private String email;
    private String phone;
    private String imageThumb;
    private String imageMed;
    private String image;

    public Contact(String firstName, String lastName, String street, String city, String email, String phone, String imageThumb, String imageMed, String image) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.street = street;
        this.city = city;
        this.email = email;
        this.phone = phone;
        this.imageThumb = imageThumb;
        this.imageMed = imageMed;
        this.image = image;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getImageThumb() {
        return imageThumb;
    }

    public void setImageThumb(String imageThumb) {
        this.imageThumb = imageThumb;
    }

    public String getImageMed() {
        return imageMed;
    }

    public void setImageMed(String imageMed) {
        this.imageMed = imageMed;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
