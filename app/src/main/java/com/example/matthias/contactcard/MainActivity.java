package com.example.matthias.contactcard;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements OnContactUIUpdate{

    ListView contactListView;
    ContactListAdapter contactArrayAdapter;
    ArrayList<Contact> contacts = new ArrayList<>();
    ContactGenerator contactGenerator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        contactGenerator = new ContactGenerator(this, getApplicationContext());

        for (int i = 0; i <= 10; i++) {
            contactGenerator.getNewContact();
        }

        contacts = contactGenerator.getContacts();

        contactListView = findViewById(R.id.contactList);
        contactArrayAdapter = new ContactListAdapter(getApplicationContext(), contacts);

        contactListView.setAdapter(contactArrayAdapter);
        contactArrayAdapter.notifyDataSetChanged();

        contactListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent newActivity = new Intent(getApplicationContext(), ContactActivity.class);

                Contact c = contacts.get(i);
                newActivity.putExtra("CONTACT_OBJECT", c);

                startActivity(newActivity);
            }
        });

    }


    @Override
    public void onContactUIUpdate() {
        contactArrayAdapter.notifyDataSetChanged();
    }

}
