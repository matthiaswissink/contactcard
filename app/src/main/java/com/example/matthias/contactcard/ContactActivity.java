package com.example.matthias.contactcard;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;


/**
 * Created by Matthias on 04-Oct-17.
 */

public class ContactActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_detail);

        Contact contact = (Contact) getIntent().getSerializableExtra("CONTACT_OBJECT");

        ImageView image = findViewById(R.id.detailContactImage);
        TextView name = findViewById(R.id.detailContactName);
        TextView phone = findViewById(R.id.detailContactPhone);
        TextView email = findViewById(R.id.detailContactEmail);
        TextView street = findViewById(R.id.detailContactStreet);
        TextView city = findViewById(R.id.detailContactCity);

        name.setText(contact.getFirstName() + " " + contact.getLastName());
        phone.setText(contact.getPhone());
        email.setText(contact.getEmail());
        street.setText(contact.getStreet());
        city.setText(contact.getCity());
        Glide.with(getApplicationContext()).load(contact.getImage()).apply(RequestOptions.circleCropTransform()).into(image);

    }
}
