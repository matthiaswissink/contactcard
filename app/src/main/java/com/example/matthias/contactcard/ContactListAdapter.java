package com.example.matthias.contactcard;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Matthias on 04-Oct-17.
 */

public class ContactListAdapter extends ArrayAdapter<Contact> {

    public ContactListAdapter(@NonNull Context context, ArrayList<Contact> contacts) {
        super(context, 0, contacts);
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.contact_list_row, parent, false);
        }

        TextView name = convertView.findViewById(R.id.contactName);
        TextView phone = convertView.findViewById(R.id.contactPhone);
        ImageView image = convertView.findViewById(R.id.contactImage);

        Contact contact = getItem(position);

        if (contact != null) {
            name.setText(contact.getFirstName() + " " + contact.getLastName());
            phone.setText(contact.getPhone());
            Glide.with(getContext()).load(contact.getImageMed()).apply(RequestOptions.circleCropTransform()).into(image);
        }

        return convertView;

    }

}
